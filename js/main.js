'use strict'

/*
Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
Технические требования:
Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.
*/

const company = {
    name: 'Company name',
    investRating: 90,
    isIPO: true,
    divisions: {
        department1: [
            {
                name: 'John',
                profession: 'middle SE',
            },
            {
                name: 'Peter',
                profession: 'senior SE',
            },
            {
                name: 'Michael',
                profession: 'teamlead',
            },
            {
                name: 'Michael',
                profession: 'QA',
            },
        ],
        department2: [
            {
                name: 'Robert',
                profession: 'accountant',
            },
            {
                name: 'Julie',
                profession: 'referent',
            },
        ],
        department3: [
            {
                name: 'Lidia',
                profession: 'CEO',
            },
            {
                name: 'Vincent',
                profession: 'CFO',
            },
        ],
    }
}

console.log(company);

function objectFullClone (obj) {
    const objClone = {};
    for (let key in obj) {
        if (typeof obj[key] !== 'object' && obj[key] !== null) {
            objClone[key] = obj[key];
        } else {
            objClone[key] = objectFullClone(obj[key]);
        }
    }

    return objClone;
}

const company2 = objectFullClone(company);

company2.name = 'Test name';

console.log(company2);